import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.html.style
import react.RProps
import react.dom.*
import react.functionalComponent
import react.useEffect
import react.useState

private val scope = MainScope()

interface CommentsProps : RProps {
    var articleId: String
}

var Comments = functionalComponent<CommentsProps> { props ->
    val (comments, setComments) = useState(emptyList<Comment>())
    useEffect(props.articleId) {
        scope.launch {
            setComments(api.getPostComments(props.articleId))
        }
    }

    div {
        comments.forEach { item ->
            div("box box--author") {
                figure("author-image") {
                    div("img") {
                        attrs.style = kotlinext.js.js {
                            backgroundImage = "url(${item.avatar})"
                        }
                        span("hidden") {
                            +"${item.name} Picture"
                        }
                    }
                }
                div("box__body") {
                    h4("box__title") {
                        +"${item.name}"
                    }
                    p("box__text") {
                        +"${item.text}"
                    }
                }
            }
        }
    }
}