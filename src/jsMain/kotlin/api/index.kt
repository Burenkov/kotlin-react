import io.ktor.client.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement

val format = Json { ignoreUnknownKeys = true }

@Serializable
data class ArticleItem(
    val id: String,
    val title: String,
    val body: String,
    val image: String,
    val created_at: String,
    val tags: List<JsonElement>
) {
    companion object {
        const val path = "/posts"
    }
}

@Serializable
data class Comment(
    val id: Int,
    val name: String,
    val avatar: String,
    val text: String
) {

}

val endpoint = "https://61391974163b56001703a24a.mockapi.io/api"

val jsonClient = HttpClient {
    install(JsonFeature) {
        serializer = KotlinxSerializer(format)
    }
}

class Api {
    suspend fun getPosts(): List<ArticleItem> {
        return jsonClient.get(endpoint + ArticleItem.path)
    }
    suspend fun getPost(id: String): ArticleItem {
        return jsonClient.get("${endpoint}/posts/${id}")
    }
    suspend fun getPostComments(id: String): List<Comment> {
        return jsonClient.get("${endpoint}/posts/${id}/comments")
    }
}

var api = Api()