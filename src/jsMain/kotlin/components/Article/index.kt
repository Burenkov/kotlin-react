import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.html.id
import react.*
import react.dom.*
import react.router.dom.useParams

external interface ArticleProps : RProps {

}

external interface IParams : RProps {
    val id: String
}

private val scope = MainScope()

var Article = functionalComponent<ArticleProps> {
    val (article, setArticle) = useState<ArticleItem?>(null);
    val params = useParams<IParams>()
    useEffect(params?.id) {
        scope.launch {
            when(params?.id) {
                is String -> setArticle(api.getPost(params.id))
            }
        }
    }
    if(article == null) {
        div {  }
    }
    div("column column--center medium-10 large-10") {
        article("post") {
            header("text-center post__header") {
                h2("post__title") {
                    +"${article?.title}"
                }
                time("post__date") {
                    +"${article?.created_at}"
                }
            }
            div("post-content") {
                attrs["dangerouslySetInnerHTML"] = kotlinext.js.js {
                    __html = article?.body
                }
            }
            div("row") {
                div("column large-10") {
                    div("post__tags") {
                        article?.tags?.forEach { item ->
                            a {
                               +"$item"
                            }
                        }
                    }
                }
                div("column large-2") {
                    child(Share)
                }
            }
            if (params?.id is String) {
                hr {  }
                child(Comments) {
                    attrs {
                        articleId = params.id
                    }
                }
                hr {  }
            }
        }
    }
}