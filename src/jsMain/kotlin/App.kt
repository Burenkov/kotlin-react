import react.RBuilder
import react.RProps
import react.child
import react.dom.*
import react.functionalComponent
import react.router.dom.*

val App = functionalComponent<RProps> { _ ->
    browserRouter {
        switch {
            route("/", exact = true) {
                child(homePage)
            }
            route("/post/:id") {
                child(ArticlePage)
            }
            route("/about") {
                aboutPage()
            }
        }
    }
}