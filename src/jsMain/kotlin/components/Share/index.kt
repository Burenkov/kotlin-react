import react.RProps
import react.dom.*
import react.functionalComponent

var Share = functionalComponent<RProps> {
    ul("share-list") {
        li {
            a("share-list__link") {
                div("icon icon--ei-sc-twitter icon--s share-list__icon share-list__icon--twitter") {
                    svg("icon__cnt") {
                        custom("use") {
                            attrs["xlinkHref"] = "#ei-sc-twitter-icon"
                        }
                    }
                }
            }
        }
        li {
            a("share-list__link") {
                div("icon icon--ei-sc-facebook icon--s share-list__icon share-list__icon--facebook") {
                    svg("icon__cnt") {
                        custom("use") {
                            attrs["xlinkHref"] = "#ei-sc-facebook-icon"
                        }
                    }
                }
            }
        }
    }
}