import kotlinx.coroutines.*
import react.*
import react.dom.*
import kotlinext.js.jsObject

private val scope = MainScope()

val articles = functionalComponent<RProps> {
    val (posts, setPosts) = useState(emptyList<ArticleItem>())
    useEffect(*emptyArray()) {
        scope.launch {
            setPosts(api.getPosts())
        }
    }
    div("post-list") {
        posts.forEach { item ->
            child(
                ArticleCart,
                props = jsObject {
                    title = item.title
                    id = item.id
                    image = item.image
                    created_at = item.created_at
                }
            )
        }
    }
}
