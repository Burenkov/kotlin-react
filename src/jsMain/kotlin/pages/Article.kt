import react.*
import react.dom.div

val ArticlePage = functionalComponent<RProps> { _ ->
    div {
        layout() {
            child(
                Article
            )
        }
    }
}
