import kotlinx.html.HTMLTag
import react.*
import react.dom.*

inline fun RBuilder.custom(tagName: String, block: RDOMBuilder<HTMLTag>.() -> Unit): ReactElement = tag(block) {
    HTMLTag(tagName, it, mapOf(), null, true, false) // I dont know yet what the last 3 params mean... to lazy to look it up
}

@JsExport
fun RBuilder.layout(children: RBuilder.() -> Unit) {
    div("off-canvas-container") {
        svgs()
        header("site-header") {
            div("row") {
                div("column small-8 medium-3 large-3") {
                    h1("logo") {
                        a {
                            attrs {
                                href = "/"
                            }
                            +"Midan"
                        }
                    }
                }
                label("off-canvas-toggle") {
                    div("icon icon--ei-navicon icon--s") {
                        svg("icon__cnt") {
                            custom("use") {
                                attrs["xlinkHref"] = "#ei-navicon-icon"
                            }
                        }
                    }
                }
                div("off-canvas-content") {
                    div("column medium-9 large-9") {
                        nav("site-header__navigation navigation") {
                            ul("list-bare") {
                                li {
                                    a("page-link") {
                                        attrs {
                                            href = "/about"
                                        }
                                        +"About"
                                    }
                                }
                                li {
                                    a("page-link") {
                                        +"Style Guide"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        div("wrapper") {
            div("row") {
                children()
            }
        }
        footer("footer") {
            div("row") {
                div("column large-12 footer__section") {
                    nav("navigation") {
                        ul("list-bare footer__nav text-center") {
                            li {
                                a("page-link") {
                                    attrs {
                                        href = "/about"
                                    }
                                    +"About"
                                }
                            }
                            li {
                                a("page-link") {
                                    attrs {
                                        href = "style-guide"
                                    }
                                    +"Style Guide"
                                }
                            }
                            li {
                                a("subscribe-button icon-feed") {
                                    attrs {
                                        href = "rss"
                                    }
                                    +"SSR"
                                }
                            }
                        }
                    }
                }
                div("column large-12 footer__section") {
                    ul("list-bare footer__nav social-icons text-center") {
                        li {
                            a {
                                attrs {
                                    href = "http://www.twitter.com/aspirethemes"
                                    target = "_blank"
                                }
                                div("icon icon--ei-sc-twitter icon--s") {
                                    svg("icon__cnt") {
                                        custom("use") {
                                            attrs["xlinkHref"] = "#ei-sc-twitter-icon"
                                        }
                                    }
                                }
                            }
                        }
                        li {
                            a {
                                attrs {
                                    href = "http://www.twitter.com/aspirethemes"
                                    target = "_blank"
                                }
                                div("icon icon--ei-sc-twitter icon--s") {
                                    svg("icon__cnt") {
                                        custom("use") {
                                            attrs["xlinkHref"] = "#ei-sc-twitter-icon"
                                        }
                                    }
                                }
                            }
                        }
                        li {
                            a {
                                attrs {
                                    href = "http://www.twitter.com/aspirethemes"
                                    target = "_blank"
                                }
                                div("icon icon--ei-sc-twitter icon--s") {
                                    svg("icon__cnt") {
                                        custom("use") {
                                            attrs["xlinkHref"] = "#ei-sc-twitter-icon"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                div("column large-12 footer__section") {
                    div("text-center") {
                        div("font-tiny") {
                            +"© 2020 Midan"
                        }
                    }
                }
            }
        }
    }
}
