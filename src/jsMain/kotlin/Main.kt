import kotlinx.browser.document
import react.dom.render
import kotlinext.js.require
import react.child

fun main() {
    render(document.getElementById("root")) {
        child(App)
    }
}
