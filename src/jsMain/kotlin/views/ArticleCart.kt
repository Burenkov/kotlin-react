import kotlinext.js.js
import kotlinx.html.style
import react.RProps
import react.dom.*
import react.functionalComponent

external interface ArticleCartProps : RProps {
    var title: String
    var id: String
    var image: String
    var created_at: String
}

var ArticleCart = functionalComponent<ArticleCartProps> { props ->
    article("post-card-wrap column medium-6 large-4") {
        div("post-card post-card--featured") {
            a("block") {
                attrs {
                    href = "/post/${props.id}"
                }
                div("post-card__image CoverImage FlexEmbed FlexEmbed--4by3") {
                    attrs.style = js {
                        backgroundImage = "url(${props.image})"
                    }
                    span {
                        div("icon icon--ei-star icon--s post-card--featured__icon") {
                            svg("icon__cnt") {
                                custom("use") {
                                    attrs["xlinkHref"] = "#ei-star-icon"
                                }
                            }
                        }
                    }
                }
            }
            div("post-card__info") {
                div("post-card__meta") {
                    span("post-card__meta__date") {
                        +"${props.created_at}"
                    }
                }
                h2("post-card__title") {
                    a {
                        attrs {
                            href = "/post/${props.id}"
                        }
                        +"${props.title}"
                    }
                }
            }
        }
    }
}